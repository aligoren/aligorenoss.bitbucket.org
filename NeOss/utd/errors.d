module utd.errors;

import std.stdio;

void RandomError()
{
	writeln("Random number error. Please try again.");
}

void NumberFormatError()
{
	writeln("Number format error. Please convert your number.");
}

void StringConvertError()
{
	writeln("Please convert int to string.");
}

void TypeConvertError()
{
	writeln("Type Convert Error: Please change your code.");
}

void OutputCultureError()
{
	writeln("Just using English character for compiling.");
}

void UndefinedError()
{
	writeln("This variable is not defined.");
}

void ExistFileError()
{
	writeln("This file is not existing.");
}

void ExistFolderError()
{
	writeln("This folder is not existing.");
}

void DataAccessError()
{
	writeln("Database access error.");
}

void DataConnectionError()
{
	writeln("Database connection error. Please Try Again.");
}

void PlatformWinError()
{
	writeln("This library is used only under Windows.");
}

void PlatformLinError()
{
	writeln("This library is used only under Linux.");
}

void PlatformMacError()
{
	writeln("This library is used only under Mac.");
}

void InternetConnError()
{
	writeln("Internet connection error.");
}

void SystemError()
{
	writeln("System error is not defined.");
}

void RunError()
{
	writeln("Run Time Error");
}

void EmptyError()
{
	writeln("Please enter a value");
}