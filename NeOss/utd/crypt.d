module utd.crypt;

import std.stdio;
import std.random;
import std.md5;
import std.regex;
import std.math;


void Ncs(string val)
{
	string[] letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", 
		"k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "v", "y", "z", "w", "x", "q" ];
	real piNumPo = PI * PI;

	Random gen;
	auto r = uniform(11.0L, 51.0L, gen);
	auto z = (r / 2) *3 / (LOG2*LOG10E);
	auto y = (z*z/2+21551*3/2*(4)) * 2 / (PI*PI_2/PI_4);
	auto snc = (y * (2^3) / (4^3)*y/M_2_PI) + (SQRT2*SQRT1_2+M_2_SQRTPI);

	Random generate;
	auto x = uniform(21.0L,32.0L, generate);
	auto cs = ((x /2 * E) * (r - y) - (y * y) * 3* (cbrt(27)) * piNumPo);
	auto sc = (cs * LN2) * (cs * LN10) * (cs * E * LN2 * LN2 / E) / (PI_4 * SQRT2);

	string valRet = getDigestString(val);

	Random let;
	auto letn = uniform(0, 26, let);

	//string letRet = getDigestString(letters[cast(uint)letn]);
	//writeln(letters[uniform(0, $)]);
	//letters[uniform(0, $)].writeln();
	//writeln(letters[cast(uint)letn]);
	writeln(letters[uniform(0, $)],cast(ulong)(snc),letters[uniform(0, $)],
	valRet,cast(ulong)(sc),letters[uniform(0, $)]);

}